﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using PetaPoco;

namespace Tunynet.Spacebuilder
{
    public class ContentCategoryPortal : Tunynet.CMS.ContentCategory, IEntity
    {
        #region 需持久化属性

        /// <summary>
        /// 继承父栏目设置
        /// </summary>
        [Ignore]
        public bool IsInherit
        {
            get
            {
                return GetExtendedProperty<bool>("IsInherit");
            }
            set
            {
                SetExtendedProperty("IsInherit", value);
            }
        }

        #endregion 需持久化属性
    }
}