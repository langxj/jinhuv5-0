﻿using System.ComponentModel.DataAnnotations;

namespace Tunynet.Spacebuilder
{
    public class SearchWordModel
    {
        /// <summary>
        /// 搜索词Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 搜索词
        /// </summary>
        [Display(Name = "搜索词")]
        [Required(ErrorMessage = "搜索词为必填项")]
        public string word { get; set; }

        /// <summary>
        /// 搜索类型
        /// </summary>
        [Display(Name = "搜索类型")]
        [Required(ErrorMessage = "请选择一个搜索类型")]
        public string SearchTypeCode { get; set; }

        /// <summary>
        /// 近6个月累计增加搜索次数
        /// </summary>
        [Display(Name = " 近6个月累计增加搜索次数")]
        [RegularExpression("^-?\\d+$", ErrorMessage = "请输入整数")]
        public int SixSearchWordCounts { get; set; }
    }
}