SET FOREIGN_KEY_CHECKS=0;

START TRANSACTION;
-- 操作日志升级

INSERT  `tn_OperationLogs` ( `TenantTypeId`, `OperationType`, `OperationObjectId`, `OperationObjectName`, `Description`,  `OperationUserId`, `Operator`, `OperatorIP`, `AccessUrl`, `DateCreated`) 
SELECT 
	   CASE  `Source` 
	   WHEN  '贴吧' THEN '100003' 
	   WHEN  '贴子' THEN '100002'
	   WHEN  '资讯' THEN '100011'
	   WHEN  '评论' THEN '000031'
	   ELSE `Source` END as `TenantTypeId`
      ,`OperationType`
      ,`OperationObjectId`
	  ,`OperationObjectName`
      ,`Description`
	 
      ,`OperatorUserId`
      ,`Operator`
      ,`OperatorIP`
      ,`AccessUrl`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
  FROM `tn.jinhudemoNew`. `tn_OperationLogs`
  where   `Source` = '评论';
   
  -- 附件下载记录升级
DELETE FROM  `tn_AttachmentAccessRecords`;

insert  `tn_AttachmentAccessRecords`( `Id`
      ,`AttachmentId`
      ,`AccessType`
      ,`UserId`
      ,`UserDisplayName`
      ,`Price`
      ,`LastDownloadDate`
      ,`DownloadDate`
      ,`IP`)
	  SELECT  `Id`
      ,`AttachmentId`
      ,0 as `AccessType`
      ,`UserId`
      ,`UserDisplayName`
      ,`Price`
      ,`LastDownloadDate`
      ,date_sub(`DownloadDate`,interval -8 hour)  as `DownloadDate`
      ,`IP`
  FROM `tn.jinhudemoNew`. `tn_AttachmentDownloadRecords`;



 
-- 友情链接升级
DELETE FROM  `tn_Links`;

INSERT  `tn_Links` (`LinkId`, `LinkName`, `LinkUrl`, `ImageAttachmentId`, `Description`, `IsEnabled`, `DisplayOrder`, `DateCreated`, `PropertyNames`, `PropertyValues`)
SELECT  `LinkId`
      ,`LinkName`
      ,`LinkUrl`
	  ,0 as `ImageAttachmentId`
      ,`Description`
      ,`IsEnabled`
      ,`DisplayOrder`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`. `spb_Links`;


  -- 类别升级
INSERT `tn_Categories` (`CategoryId`
      ,`ParentId`
      ,`OwnerId`
      ,`TenantTypeId`
      ,`CategoryName`
      ,`Description`
      ,`DisplayOrder`
      ,`Depth`
      ,`ChildCount`
      ,`ItemCount`
      ,`ImageAttachmentId`
      ,`LastModified`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT `CategoryId`
      ,`ParentId`
      ,`OwnerId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`CategoryName`
      ,`Description`
      ,`DisplayOrder`
      ,`Depth`
      ,`ChildCount`
      ,`ItemCount`
      ,0 as `ImageAttachmentId`
      ,`LastModified`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Categories` where `TenantTypeId` in('000071') ;

-- 类别和内容关联升级
DELETE FROM `tn_ItemsInCategories`;
INSERT `tn_ItemsInCategories` (`Id`
      ,`CategoryId`
      ,`ItemId`)
SELECT  `Id`
      ,`tn_ItemsInCategories`.`CategoryId`
      ,`ItemId`
  FROM `tn.jinhudemoNew`.`tn_ItemsInCategories` left join `tn.jinhudemoNew`.`tn_Categories`  on  `tn.jinhudemoNew`.`tn_Categories`.`CategoryId`  =`tn_ItemsInCategories`.`CategoryId` where  `tn.jinhudemoNew`.`tn_Categories`.TenantTypeId in('000071');


-- 用户升级
DELETE FROM  `tn_Users`;
insert   `tn_Users` (`UserId`
      ,`UserName`
      ,`Password`
      ,`HasAvatar`
      ,`HasCover`
      ,`PasswordFormat`
      ,`AccountEmail`
      ,`IsEmailVerified`
      ,`AccountMobile`
      ,`IsMobileVerified`
      ,`TrueName`
      ,`ForceLogin`
      ,`Status`
      ,`DateCreated`
      ,`IpCreated`
      ,`UserType`
      ,`LastActivityTime`
      ,`LastAction`
      ,`IpLastActivity`
      ,`IsBanned`
      ,`BanReason`
      ,`BanDeadline`
      ,`IsModerated`
      ,`IsForceModerated`
      ,`DatabaseQuota`
      ,`DatabaseQuotaUsed`
      ,`IsUseCustomStyle`
      ,`FollowedCount`
      ,`FollowerCount`
      ,`ExperiencePoints`
      ,`ReputationPoints`
      ,`TradePoints`
      ,`TradePoints2`
      ,`TradePoints3`
      ,`TradePoints4`
      ,`FrozenTradePoints`
      ,`Rank`
      ,`AuditStatus`
      )

SELECT  `UserId`
      ,`UserName`
      ,`Password`
	  ,  CASE LENGTH(`Avatar`)>14
	   WHEN 0 THEN 0
	   ELSE 1 END as `HasAvatar`
	   ,0 as `HasCover`
      ,`PasswordFormat`
      ,`AccountEmail`
      ,`IsEmailVerified`
      ,`AccountMobile`
      ,`IsMobileVerified`
      ,`TrueName`
      ,`ForceLogin`
      ,`IsActivated`as `Status`
      ,`DateCreated`
      ,`IpCreated`
      ,`UserType`
      ,`LastActivityTime`
      ,`LastAction`
      ,`IpLastActivity`
      ,`IsBanned`
      ,`BanReason`
      ,`BanDeadline`
      ,`IsModerated`
      ,`IsForceModerated`
      ,`DatabaseQuota`
      ,`DatabaseQuotaUsed`
      ,`IsUseCustomStyle`
      ,`FollowedCount`
      ,`FollowerCount`
      ,`ExperiencePoints`
      ,`ReputationPoints`
      ,`TradePoints`
      ,`TradePoints2`
      ,`TradePoints3`
      ,`TradePoints4`
      ,`FrozenTradePoints`
      ,`Rank`
	  ,40 as `AuditStatus`
    
  FROM `tn.jinhudemoNew`. `tn_Users`;
-- 用户资料升级
DELETE FROM  `spb_UserProfiles`;
INSERT  `spb_UserProfiles` (`UserId`, `Gender`, `BirthdayType`, `Birthday`, `LunarBirthday`, `NowAreaCode`, `QQ`, `CardType`, `CardID`, `Introduction`, `PropertyNames`, `PropertyValues`, `Integrity`) 
SELECT `UserId`
      ,`Gender`
      ,`BirthdayType`
      ,`Birthday`
      ,`LunarBirthday`
      ,`NowAreaCode`
      ,`QQ`
      ,`CardType`
      ,`CardID`
      ,`Introduction`
      ,`PropertyNames`
      ,`PropertyValues`
      ,`Integrity`
  FROM `tn.jinhudemoNew`.`spb_Profiles`;
  -- 第三方账号绑定升级
DELETE FROM  `tn_AccountBindings`;

INSERT  `tn_AccountBindings` (`Id`
      ,`UserId`
      ,`AccountTypeKey`
      ,`Identification`
      ,`AccessToken`
	  ,`ExpiredDate` )
	  SELECT  `Id`
      ,`UserId`
      ,`AccountTypeKey`
      ,`Identification`
      ,`AccessToken`
	  ,NOW() as `ExpiredDate`	
	   FROM `tn.jinhudemoNew`. `tn_AccountBindings`;

   -- 第三方账号类型升级
DELETE FROM  `tn_AccountTypes`;
INSERT  `tn_AccountTypes` ( `AccountTypeKey`
      ,`ThirdAccountGetterClassType`
      ,`AppKey`
      ,`AppSecret`
      ,`IsEnabled`)
SELECT `AccountTypeKey`
      , 'Tunynet.Spacebuilder.QQAccountGetter,Tunynet.AccountBindings' as `ThirdAccountGetterClassType`
      ,`AppKey`
      ,`AppSecret`
      ,`IsEnabled`
  FROM `tn.jinhudemoNew`. `tn_AccountTypes` where   `tn_AccountTypes`.`AccountTypeKey` ='QQ';
    -- 关注升级
DELETE FROM  `tn_Follows`;

INSERT  `tn_Follows` ( `Id`
      ,`UserId`
      ,`FollowedUserId`
      ,`NoteName`
      ,`IsQuietly`
      ,`IsNewFollower`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
      ,`IsMutual`
      ,`LastContactDate`)
	  SELECT  `Id`
      ,`UserId`
      ,`FollowedUserId`
      ,`NoteName`
      ,`IsQuietly`
      ,`IsNewFollower`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
      ,`IsMutual`
      ,`LastContactDate`
  FROM `tn.jinhudemoNew`. `tn_Follows`;


-- 邀请码升级
DELETE FROM  `tn_InvitationCodes`;
INSERT  `tn_InvitationCodes` ( `Code`
      ,`UserId`
      ,`IsMultiple`
      ,`ExpiredDate`
      ,`DateCreated`)
	 SELECT `Code`
      ,`UserId`
      ,`IsMultiple`
      ,`ExpiredDate`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
  FROM `tn.jinhudemoNew`. `tn_InvitationCodes`;

-- 邀请好友记录升级
DELETE FROM  `tn_InviteFriendRecords`;
 
INSERT  `tn_InviteFriendRecords` ( `Id`
      ,`UserId`
      ,`InvitedUserId`
      ,`Code`
      ,`DateCreated`
      ,`IsRewarded`)
SELECT `Id`
      ,`UserId`
      ,`InvitedUserId`
      ,`Code`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`InvitingUserHasBeingRewarded` as `IsRewarded`
  FROM `tn.jinhudemoNew`. `tn_InviteFriendRecords`;

    -- 资讯热词搜索

INSERT .`tn_SearchWords` (`id`,
       `Word`
      ,`SearchTypeCode`
      ,`IsAddedByAdministrator`
      ,`DateCreated`
      ,`LastModified`)
	  SELECT  `id`,
      `Term` as `Word`
      ,'All' as `SearchTypeCode`
      ,`IsAddedByAdministrator`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`LastModified`
  FROM `tn.jinhudemoNew`.`tn_SearchedTerms` where SearchTypeCode = 'GlobalSearcher';

-- 积分记录升级
DELETE FROM  `tn_PointRecords`;

INSERT  `tn_PointRecords` ( `RecordId`
      ,`UserId`
      ,`OperatorUserId`
      ,`PointItemName`
      ,`Description`
      ,`ExperiencePoints`
      ,`ReputationPoints`
      ,`TradePoints`
      ,`TradePoints2`
      ,`TradePoints3`
      ,`TradePoints4`
      ,`DateCreated`)
SELECT `RecordId`
      ,`UserId`
	  ,0 as `OperatorUserId`
      ,`PointItemName`
      ,`Description`
      ,`ExperiencePoints`
      ,`ReputationPoints`
      ,`TradePoints`
      ,`TradePoints2`
      ,`TradePoints3`
      ,`TradePoints4`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
  FROM `tn.jinhudemoNew`. `tn_PointRecords`;


 



-- 管理员角色保留初始管理员
DELETE FROM  `tn_UsersInRoles`;

INSERT  `tn_UsersInRoles` (
       `Id`
      ,`UserId`
      ,`RoleId`)
SELECT  `Id`
      ,`UserId`
      ,101 as `RoleId`
  FROM `tn.jinhudemoNew`. `tn_UsersInRoles` where  RoleName='SuperAdministrator';


  drop procedure if exists StatisticStore;  
CREATE PROCEDURE StatisticStore()  
BEGIN  
    -- 创建接收游标数据的变量  
DECLARE n varchar(50);
DECLARE TypeId int;
   
    -- 创建结束标志变量  
    declare done int default false;  
    -- 创建游标  
    declare cur cursor for select table_name from information_schema.tables where table_schema='tn.jinhudemoNew' and table_type='base table' and table_name  in ('tn_Counts_000031','tn_Counts_101202','tn_Counts_101301','tn_Counts_101302','tn_Counts_101501');  
    -- 指定游标循环结束时的返回值  
    declare continue HANDLER for not found set done = true;  

 -- 打开游标  
    open cur;  
    -- 开始循环游标里的数据  
    read_loop:loop  
    -- 根据游标当前指向的一条数据  
    fetch cur into n;  
    -- 判断游标的循环是否结束  
    if done then  
        leave read_loop;    -- 跳出游标循环  
    end if;  
    -- 获取一条数据时，将count值进行累加操作，这里可以做任意你想做的操作


 

set TypeId =right(n,6);
	 set TypeId = 	CASE TypeId 
     	WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE TypeId END;

SET @tablename = concat('  from `tn.jinhudemoNew`.',n);  
set @additional =concat(TypeId,' as TenantTypeId') ;
SET @strsql = concat('INSERT  `tn_Counts` (
       `OwnerId`
      ,`ObjectId`
      ,`CountType`
      ,`StatisticsCount` 
	  ,`TenantTypeId`)    select `OwnerId`,`ObjectId`,`CountType`,`StatisticsCount`, ',@additional, @tablename);  
	

PREPARE stmt FROM @strsql;  
EXECUTE stmt;




    -- 结束游标循环  
    end loop;  
    -- 关闭游标  
    close cur;  
  
    -- 输出结果  a
 
END;  
-- 调用存储过程  
call StatisticStore();  



   -- --每日计数升级
DELETE FROM  `tn_CountsPerDay`;
drop procedure if exists StatisticStore;  
CREATE PROCEDURE StatisticStore()  
BEGIN  
    -- 创建接收游标数据的变量  
DECLARE n varchar(50);
DECLARE TypeId int;
   
    -- 创建结束标志变量  
    declare done int default false;  
    -- 创建游标  
    declare cur cursor for select table_name from information_schema.tables where table_schema='tn.jinhudemoNew' and table_type='base table' and table_name  in ('tn_CountsPerDay_000031','tn_CountsPerDay_101202','tn_CountsPerDay_101301','tn_CountsPerDay_101302','tn_CountsPerDay_101501');

    -- 指定游标循环结束时的返回值  
    declare continue HANDLER for not found set done = true;  

 -- 打开游标  
    open cur;  
    -- 开始循环游标里的数据  
    read_loop:loop  
    -- 根据游标当前指向的一条数据  
    fetch cur into n;  
    -- 判断游标的循环是否结束  
    if done then  
        leave read_loop;    -- 跳出游标循环  
    end if;  
    -- 获取一条数据时，将count值进行累加操作，这里可以做任意你想做的操作

set TypeId =right(n,6);
	 set TypeId = 	CASE TypeId 
     	WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE TypeId END;

SET @tablename = concat('  from `tn.jinhudemoNew`.',n);  
set @additional =concat(TypeId,' as TenantTypeId') ;
 


SET @strsql = concat('INSERT  `tn_CountsPerDay` (
      `OwnerId`
      ,`ObjectId`
      ,`CountType`
      ,`ReferenceYear`
      ,`ReferenceMonth`
      ,`ReferenceDay`
      ,`StatisticsCount`,
	  `TenantTypeId`)   select  `OwnerId`,`ObjectId`,`CountType`,`ReferenceYear`,`ReferenceMonth`,`ReferenceDay`,`StatisticsCount`, ',@additional, @tablename);  
	

PREPARE stmt FROM @strsql;  
EXECUTE stmt;




    -- 结束游标循环  
    end loop;  
    -- 关闭游标  
    close cur;  
  
    -- 输出结果  a
 
END;  
-- 调用存储过程  
call StatisticStore(); 



-- --用户计数升级

drop procedure if exists StatisticStore;  
CREATE PROCEDURE StatisticStore()  
BEGIN  
    -- 创建接收游标数据的变量  
DECLARE n varchar(60);
DECLARE pendingCount int;
DECLARE againCount int;
DECLARE successCount int;
DECLARE returnCount int;
DECLARE approvalStatus tinyint;
   
    -- 创建结束标志变量  
    declare done int default false;  
    -- 创建游标  
    declare cur cursor for SELECT  `UserId` FROM  `tn_Users`;

    -- 指定游标循环结束时的返回值  
    declare continue HANDLER for not found set done = true;  

 -- 打开游标  
    open cur;  
    -- 开始循环游标里的数据  
    read_loop:loop  
    -- 根据游标当前指向的一条数据  
    fetch cur into n;  
    -- 判断游标的循环是否结束  
    if done then  
        leave read_loop;    -- 跳出游标循环  
    end if;  
    -- 获取一条数据时，将count值进行累加操作，这里可以做任意你想做的操作

-- 贴子计数升级
	set  returnCount= (select count(*) from  tn_Threads where UserId= n );
	set  successCount= (select count(*) from  tn_Threads where UserId= n  and `tn_Threads`.ApprovalStatus = 40);
    set  againCount= (select count(*) from  tn_Threads where UserId= n  and  `tn_Threads`.ApprovalStatus = 30);
	set  pendingCount= (select count(*) from  tn_Threads where UserId= n  and `tn_Threads`.ApprovalStatus = 20);
	 	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Post-TenantTypeId-100002-ThreadCount-UserId-',n),returnCount,NOW());
	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Post-TenantTypeId-100002-ThreadCount-UserId-',n,'-AuditStatusSuccess'),successCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Post-TenantTypeId-100002-ThreadCount-UserId-',n,'-AuditStatusAgain'),againCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Post-TenantTypeId-100002-ThreadCount-UserId-',n,'-AuditStatusPending'),pendingCount,NOW());


	

  -- 资讯文章计数升级
	set  returnCount= (select count(*) from  tn_ContentItems where UserId= n and tn_ContentItems.ContentModelId = 5 );
	set  successCount= (select count(*) from  tn_ContentItems where UserId= n  and tn_ContentItems.ApprovalStatus = 40 and tn_ContentItems.ContentModelId = 5);
    set  againCount= (select count(*) from  tn_ContentItems where UserId= n  and tn_ContentItems.ApprovalStatus = 30 and tn_ContentItems.ContentModelId = 5);
	set  pendingCount= (select count(*) from  tn_ContentItems where UserId= n  and tn_ContentItems.ApprovalStatus = 20 and tn_ContentItems.ContentModelId = 5);
	 	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('CMS-ContentModelKey-Contribution-ContentItemCount-UserId-',n),returnCount,NOW());
	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('CMS-ContentModelKey-Contribution-ContentItemCount-UserId-',n,'-AuditStatusSuccess'),successCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('CMS-ContentModelKey-Contribution-ContentItemCount-UserId-',n,'-AuditStatusAgain'),againCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('CMS-ContentModelKey-Contribution-ContentItemCount-UserId-',n,'-AuditStatusPending'),pendingCount,NOW());
	 
	 
	  -- 评论计数升级
	set  returnCount= (select count(*) from  `tn_Comments` where UserId= n and TenantTypeId not in('100002') );
	set  successCount= (select count(*) from  `tn_Comments` where UserId= n  and tn_Comments.ApprovalStatus = 40 and TenantTypeId not in('100002') );
    set  againCount= (select count(*) from  `tn_Comments` where UserId= n  and tn_Comments.ApprovalStatus = 30 and TenantTypeId not in('100002') );
	set  pendingCount= (select count(*) from  `tn_Comments` where UserId= n  and tn_Comments.ApprovalStatus = 20 and TenantTypeId not in('100002') );
	 	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Comment-TenantTypeId--CommentCount-UserId-',n),returnCount,NOW());
	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Comment-TenantTypeId--CommentCount-UserId-',n,'-AuditStatusSuccess'),successCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Comment-TenantTypeId--CommentCount-UserId-',n,'-AuditStatusAgain'),againCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Comment-TenantTypeId-- CommentCount-UserId-',n,'-AuditStatusPending'),pendingCount,NOW());

	    -- 回贴计数升级
	set  returnCount= (select count(*) from  `tn_Comments` where UserId= n and TenantTypeId  in('100002') );
	set  successCount= (select count(*) from  `tn_Comments` where UserId= n  and tn_Comments. ApprovalStatus = 40 and TenantTypeId  in('100002') );
    set  againCount= (select count(*) from  `tn_Comments` where UserId= n  and tn_Comments.ApprovalStatus = 30 and TenantTypeId  in('100002') );
	set  pendingCount= (select count(*) from  `tn_Comments` where UserId= n  and tn_Comments.ApprovalStatus = 20 and TenantTypeId  in('100002') );
	 	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Comment-TenantTypeId-100002-CommentCount-UserId-',n),returnCount,NOW());
	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Comment-TenantTypeId-100002-CommentCount-UserId-',n,'-AuditStatusSuccess'),successCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Comment-TenantTypeId-100002-CommentCount-UserId-',n,'-AuditStatusAgain'),againCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Comment-TenantTypeId-100002-CommentCount-UserId-',n,'-AuditStatusPending'),pendingCount,NOW());
	 

	   -- 收藏计数升级
	set  returnCount= (select count(*) from  `tn_Favorites` where UserId= n );
	
	 	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Favorite-FavoriteCount-UserId-',n),returnCount,NOW());

	   -- 点赞计数升级
	set  returnCount= (select count(*) from  tn_AttitudeRecords where UserId= n  );
	
	 	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Attitude-AttitudeCount-UserId-',n),returnCount,NOW());

	





    -- 结束游标循环  
    end loop;  
    -- 关闭游标  
    close cur;  
  
    -- 输出结果  a
 
END;  
-- 调用存储过程  
call StatisticStore();  

  -- 搜索词计数
insert  tn_Counts (
       `TenantTypeId`
      ,`OwnerId`
      ,`ObjectId`
      ,`CountType`
      ,`StatisticsCount`)

SELECT 
      '000141'as  `TenantTypeId`
	  ,`OwnerId`
      ,`ObjectId`
	   ,'SearchWordCounts' as `CountType`
      ,`StatisticsCount`
     
  FROM `tn.jinhudemoNew`.`tn_Counts_000021` as tb  where tb.CountType='SearchCount' ;
  
  insert  tn_Counts (
       `TenantTypeId`
      ,`OwnerId`
      ,`ObjectId`
      ,`CountType`
      ,`StatisticsCount`)


SELECT 
      '000141'as  `TenantTypeId`
	  ,`OwnerId`
      ,`ObjectId`
	   ,'SearchWordCounts-180' as `CountType`
      ,`StatisticsCount`	
     
  FROM `tn.jinhudemoNew`.`tn_Counts_000021` as tb  where tb.CountType='SearchCount-7';

   COMMIT;