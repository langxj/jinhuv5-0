begin tran
----内容项升级
SET IDENTITY_INSERT [dbo].[tn_ContentItems] ON
INSERT [dbo].[tn_ContentItems] (
      [ContentItemId]
      ,[ContentCategoryId]
      ,[ContentModelId]
      ,[Subject]
      ,[FeaturedImageAttachmentId]
      ,[DepartmentGuid]
      ,[Points]
      ,[UserId]
      ,[Author]
      ,[Body]
      ,[Summary]
      ,[IsLocked]
      ,[IsSticky]
      ,[ApprovalStatus]
      ,[IP]
      ,[DatePublished]
      ,[DateCreated]
      ,[LastModified]
	   ,[PropertyNames]
      ,[PropertyValues]
     )
SELECT 
        ThreadId+1000000 as [ContentItemId],
       1 as [ContentCategoryId]
       ,5  as [ContentModelId]
	   , [Subject]
      ,[FeaturedImageAttachmentId]
	   ,'' as  [DepartmentGuid]
	   ,0 as [Points]
	  ,[UserId]
      ,[Author]
      ,[Body]
	  , [Summary]
	  , [IsLocked]
	    , [IsSticky]
      ,[AuditStatus] as [ApprovalStatus]
      ,[IP]
      , dateadd(hour,8,[DateCreated]) as [DatePublished]
      , dateadd(hour,8,[DateCreated]) as [DateCreated]
      , dateadd(hour,8,[DateCreated]) as [LastModified]
     ,'IsVisible:S:0:4:' as [PropertyNames]
      ,'True' as [PropertyValues]
     FROM [tn.jinhudemoNew].[dbo].[spb_BlogThreads]
	  SET IDENTITY_INSERT [dbo].[tn_ContentItems] OFF
-- 更新栏目下的内容数量
	  update  [tn_ContentCategories] set [ChildCount] = [ChildCount]+(SELECT count( [ThreadId])
  FROM [tn.jinhudemoNew].[dbo].[spb_BlogThreads])
 where  [CategoryId] =1

 

	  -- 微博日志升级
  INSERT [dbo].[tn_OperationLogs] ( [TenantTypeId], [OperationType], [OperationObjectId], [OperationObjectName], [Description], [OperationUserRole], [OperationUserId], [Operator], [OperatorIP], [AccessUrl], [DateCreated]) 
SELECT 
	   CASE  [Source] 
	   WHEN  '贴吧' THEN '100003' 
	   WHEN  '贴子' THEN '100002'
	   WHEN  '资讯' THEN '100011'
	   WHEN  '微博' THEN '100011'
	   WHEN  '评论' THEN '000031'
	   ELSE [Source] END as [TenantTypeId]
      ,[OperationType]
      ,[OperationObjectId]
	  ,[OperationObjectName]
      ,[Description]
	  ,' ' as [OperationUserRole]
      ,[OperatorUserId]
      ,[Operator]
      ,[OperatorIP]
      ,[AccessUrl]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
  FROM [tn.jinhudemoNew].[dbo].[tn_OperationLogs]
  where  [Source] = '微博'

   -----附件升级
 SET IDENTITY_INSERT [dbo].[tn_Attachments] ON
INSERT [dbo].[tn_Attachments] ([AttachmentId], [AssociateId], [OwnerId], [TenantTypeId], [UserId], [UserDisplayName], [FileName], [FriendlyFileName], [MediaType], [ContentType], [FileLength], [Price], [IP], [ConvertStatus], [DateCreated], [Discription], [IsShowInAttachmentList], [PropertyNames], [PropertyValues], [DisplayOrder]) 
SELECT [AttachmentId]
      ,[AssociateId]+1000000
      ,[OwnerId]
      ,
	   CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100201' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[UserId]
      ,[UserDisplayName]
      ,[FileName]
      ,[FriendlyFileName]
      ,[MediaType]
      ,[ContentType]
      ,[FileLength]
      ,[Price]
      ,[IP]
	  ,0 as [ConvertStatus]
      , [DateCreated]
	  ,'' as [Discription]
	  ,0 as [IsShowInAttachmentList]
      ,[PropertyNames]
      ,[PropertyValues]
	  ,[AttachmentId] as [DisplayOrder]
  FROM [tn.jinhudemoNew].[dbo].[tn_Attachments] where [TenantTypeId] in ('100201') 
  
  SET IDENTITY_INSERT [dbo].[tn_Attachments] OFF
   -----收藏升级
  insert [dbo].[tn_Favorites]( 
      [TenantTypeId]
      ,[UserId]
      ,[ObjectId])
SELECT   
       CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002'
	   WHEN  '101201' THEN '100003'  
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100201' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[UserId]
      ,[ObjectId]+1000000
  FROM [tn.jinhudemoNew].[dbo].[tn_Favorites] where [TenantTypeId] in('100201') 

  -----评论升级
SET IDENTITY_INSERT [dbo].[tn_Comments] ON
INSERT [dbo].[tn_Comments] ([Id]
      ,[ParentIds]
      ,[ParentId]
      ,[CommentedObjectId]
      ,[TenantTypeId]
      ,[CommentType]
      ,[ChildrenCount]
      ,[OwnerId]
      ,[UserId]
      ,[Author]
      ,[Subject]
      ,[Body]
      ,[IsAnonymous]
      ,[IsPrivate]
      ,[ApprovalStatus]
      ,[IP]
      ,[DateCreated]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT  [Id]
      ,CASE  
	   WHEN  [ParentId]>0 THEN '0,'+(CAST([ParentId] as nvarchar(125)) +',')
	   ELSE '0,' END as [ParentIds] 
	  
	  
      ,[ParentId]
      ,[CommentedObjectId]+1000000
      , CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100201' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
	  ,'' as [CommentType]
	  ,[ChildCount] as [ChildrenCount]
      ,[OwnerId]
      ,[UserId]
      ,[Author]
      ,[Subject]
      ,[Body]
      ,[IsAnonymous]
      ,[IsPrivate]
      ,[AuditStatus] as [ApprovalStatus]
      ,[IP]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[tn_Comments] where [TenantTypeId] in('100201') 
  SET IDENTITY_INSERT [dbo].[tn_Comments] OFF
  -----点赞升级
SET IDENTITY_INSERT [dbo].[tn_Attitudes] ON
INSERT [dbo].[tn_Attitudes] ([Id]
      , [TenantTypeId]
      ,[ObjectId]
      ,[SupportCount])
SELECT [Id]
       , CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100201' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[ObjectId]+1000000
      ,[SupportCount]
  FROM [tn.jinhudemoNew].[dbo].[tn_Attitudes] where [TenantTypeId] in('100201') 
SET IDENTITY_INSERT [dbo].[tn_Attitudes] OFF
-----点赞记录升级
SET IDENTITY_INSERT [dbo].[tn_AttitudeRecords] ON
INSERT [dbo].[tn_AttitudeRecords] ([Id]
      ,[TenantTypeId]
      ,[ObjectId]
      ,[UserId])
SELECT  [Id]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100201' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[ObjectId]+1000000
      ,[UserId]
  FROM [tn.jinhudemoNew].[dbo].[tn_AttitudeRecords] where [TenantTypeId] in('100201') 
SET IDENTITY_INSERT [dbo].[tn_AttitudeRecords] OFF

-----微博 精华升级
INSERT [dbo].[tn_SpecialContentItems] (
       [TenantTypeId]
      ,[TypeId]
      ,[RegionId]
      ,[ItemId]
      ,[ItemName]
      ,[FeaturedImageAttachmentId]
      ,[Recommender]
      ,[RecommenderUserId]
      ,[DateCreated]
      ,[ExpiredDate]
      ,[DisplayOrder]
  )
SELECT  
      '101501' as [TenantTypeId]
      ,11 as [TypeId]
      ,0 as [RegionId]
      ,[ThreadId]+1000000 as  [ItemId]
	  ,[Subject] as [ItemName]
      , 0 as [FeaturedImageAttachmentId]
      ,' ' as [Recommender]
	  ,0 as [RecommenderUserId]
      ,  dateadd(hour,8,[DateCreated]) as [DateCreated] 
	  , dateadd(y,20,getdate()) as [ExpiredDate]
	  , [ThreadId] as  [DisplayOrder]
  FROM [tn.jinhudemoNew].[dbo].[spb_BlogThreads] where IsEssential=1

-----类别升级
SET IDENTITY_INSERT [dbo].[tn_Categories] ON
INSERT [dbo].[tn_Categories] ([CategoryId]
      ,[ParentId]
      ,[OwnerId]
      ,[TenantTypeId]
      ,[CategoryName]
      ,[Description]
      ,[DisplayOrder]
      ,[Depth]
      ,[ChildCount]
      ,[ItemCount]
      ,[ImageAttachmentId]
      ,[LastModified]
      ,[DateCreated]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT [CategoryId]
      ,[ParentId]
      ,[OwnerId]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100201' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[CategoryName]
      ,[Description]
      ,[DisplayOrder]
      ,[Depth]
      ,[ChildCount]
      ,[ItemCount]
      ,0 as [ImageAttachmentId]
      ,[LastModified]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[tn_Categories] where [TenantTypeId] in('100201') 
SET IDENTITY_INSERT [dbo].[tn_Categories] OFF

-----类别和内容关联升级
SET IDENTITY_INSERT [dbo].[tn_ItemsInCategories] ON
INSERT [dbo].[tn_ItemsInCategories] ([Id]
      ,[CategoryId]
      ,[ItemId])
SELECT  [Id]
      ,[tn_ItemsInCategories].[CategoryId]
      ,[ItemId]+1000000
  FROM [tn.jinhudemoNew].[dbo].[tn_ItemsInCategories] left join [tn.jinhudemoNew].[dbo].[tn_Categories]  on  [tn.jinhudemoNew].[dbo].[tn_Categories].[CategoryId]  =[tn_ItemsInCategories].[CategoryId] where  [tn.jinhudemoNew].[dbo].[tn_Categories].TenantTypeId in('100201')
SET IDENTITY_INSERT [dbo].[tn_ItemsInCategories] OFF




-----标签升级
SET IDENTITY_INSERT [dbo].[tn_Tags] ON
INSERT [dbo].[tn_Tags] ([TagId]
      ,[TenantTypeId]
      ,[TagName]
      ,[Description]
      ,[ImageAttachmentId]
      ,[IsFeatured]
      ,[ItemCount]
      ,[DateCreated]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT [TagId]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100201' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[TagName]
      ,[Description]
      ,0 as  [ImageAttachmentId]
      ,[IsFeatured]
      ,[ItemCount]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[tn_Tags] where [TenantTypeId] in('100201') 
SET IDENTITY_INSERT [dbo].[tn_Tags] OFF


-----标签关联项升级
SET IDENTITY_INSERT [dbo].[tn_ItemsInTags] ON
INSERT [dbo].[tn_ItemsInTags] ([Id]
      ,[TagName]
      ,[ItemId]
      ,[TenantTypeId])
SELECT  [Id]
      ,[TagName]
      ,[ItemId]+1000000
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
  FROM [tn.jinhudemoNew].[dbo].[tn_ItemsInTags] where [TenantTypeId] in('100201') 
SET IDENTITY_INSERT [dbo].[tn_ItemsInTags] OFF



-----微博热词更新
SET IDENTITY_INSERT [dbo].[tn_SearchWords] ON
INSERT [dbo].[tn_SearchWords] (
    id,
       [Word]
      ,[SearchTypeCode]
      ,[IsAddedByAdministrator]
      ,[DateCreated]
      ,[LastModified])
	  SELECT  
	  id,
      [Term] as [Word]
      ,'Cms' as [SearchTypeCode]
      ,[IsAddedByAdministrator]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[LastModified]
  FROM [tn.jinhudemoNew].[dbo].[tn_SearchedTerms] where SearchTypeCode = 'BlogSearcher'
  SET IDENTITY_INSERT [dbo].[tn_SearchWords] OFF
 commit tran