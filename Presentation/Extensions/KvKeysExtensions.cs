﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Common;

namespace Tunynet.Common
{
    /// <summary>
    /// KvKey管理方法
    /// </summary>
    public static class KvKeysExtensions
    {
        /// <summary>
        /// 获取用户的提问计数
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <param name="userId"></param>
        /// <param name="auditStatus"></param>
        /// <returns></returns>
        public static string UserAskQuestionCount(this KvKeys kvKeys, long userId, AuditStatus? auditStatus = null)
        {
            var key = "Ask-QuestionCount-UserId-" + userId;
            if (auditStatus.HasValue)
                key = key + "-AuditStatus" + auditStatus.Value;
            return key;
        }

        /// <summary>
        /// 获取用户的问题被采纳计数
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string UserAnswerAcceptCount(this KvKeys kvKeys, long userId)
        {
            return "Ask-AnswerAcceptCount-UserId-" + userId;
        }

        /// <summary>
        /// 获取用户的问答计数
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <param name="userId"></param>
        /// <param name="auditStatus"></param>
        /// <returns></returns>
        public static string UserAskAnswerCount(this KvKeys kvKeys, long userId, AuditStatus? auditStatus = null)
        {
            var key = "Ask-AnswerCount-UserId-" + userId;
            if (auditStatus.HasValue)
                key = key + "-AuditStatus" + auditStatus.Value;
            return key;
        }

        /// <summary>
        /// 问答全文索引Key
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <returns></returns>
        public static string AskSearch(this KvKeys kvKeys)
        {
            return "AskSearch";
        }

        /// <summary>
        /// 问答全文索引需要删除索引Key
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <returns></returns>
        public static string AskDeleteSearch(this KvKeys kvKeys)
        {
            return "AskDeleteSearch";
        }

        /// <summary>
        /// 问答全文索引更新Key
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <returns></returns>
        public static string AskUpdateSearch(this KvKeys kvKeys)
        {
            return "AskUpdateSearch";
        }


        /// <summary>
        /// 获取用户收藏的文档数量
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <param name="userId"></param>
        /// <param name="auditStatus"></param>
        /// <returns></returns>
        public static string UserDocumentFavoriteCount(this KvKeys kvKeys, long userId, AuditStatus? auditStatus = null)
        {
            var key = "DocumentFavoriteCount-UserId-" + userId;
            if (auditStatus.HasValue)
                key = key + "-AuditStatus" + auditStatus.Value;
            return key;
        }

        /// <summary>
        /// 获取用户上传的文档数量
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <param name="userId"></param>
        /// <param name="auditStatus"></param>
        /// <returns></returns>
        public static string UserDocumentUploadCount(this KvKeys kvKeys, long userId, AuditStatus? auditStatus = null)
        {
            var key = "DocumentUploadCount-UserId-" + userId;
            if (auditStatus.HasValue)
                key = key + "-AuditStatus" + auditStatus.Value;
            return key;
        }

        /// <summary>
        /// 获取用户下载的文档数量
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <param name="userId"></param>
        /// <param name="auditStatus"></param>
        /// <returns></returns>
        public static string UserDocumentDownloadCount(this KvKeys kvKeys, long userId, AuditStatus? auditStatus = null)
        {
            var key = "DocumentDownloadCount-UserId-" + userId;
            if (auditStatus.HasValue)
                key = key + "-AuditStatus" + auditStatus.Value;
            return key;
        }

        /// <summary>
        /// 文档全文索引Key
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <returns></returns>
        public static string DocSearch(this KvKeys kvKeys)
        {
            return "DocSearch";
        }

        /// <summary>
        /// 文档全文索引需要删除索引Key
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <returns></returns>
        public static string DocDeleteSearch(this KvKeys kvKeys)
        {
            return "DocDeleteSearch";
        }

        /// <summary>
        /// 文档全文索引更新Key
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <returns></returns>
        public static string DocUpdateSearch(this KvKeys kvKeys)
        {
            return "DocUpdateSearch";
        }

        /// <summary>
        /// 文档转换
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <returns></returns>
        public static string ConvertDocument(this KvKeys kvKeys)
        {
            return "ConvertDocument";
        }

        /// <summary>
        /// 文档转换次数计数
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <returns></returns>
        public static string ConvertDocumentCount(this KvKeys kvKeys)
        {
            return "ConvertDocumentCount";
        }

        /// <summary>
        /// 文档转换中的附件Id
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <returns></returns>
        public static string ConvertingId(this KvKeys kvKeys)
        {
            return "ConvertingId";
        }
        /// <summary>
        /// 获取用户的活动计数
        /// </summary>
        /// <param name="kvKeys"></param>
        /// <param name="userId"></param>
        /// <param name="auditStatus"></param>
        /// <returns></returns>
        public static string UserEventCount(this KvKeys kvKeys, long userId, AuditStatus? auditStatus = null)
        {
            var key = "Event-EventCount-UserId-" + userId;
            if (auditStatus.HasValue)
                key = key + "-AuditStatus" + auditStatus.Value;
            return key;
        }

       
    }
}