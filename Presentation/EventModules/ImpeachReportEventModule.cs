﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Events;
using Tunynet.Logging;

namespace Tunynet.Common
{
    /// <summary>
    ///用户举报事件
    /// </summary>
    public class ImpeachReportEventModule : IEventMoudle
    {
        private OperationLogService operationLogService;
        private RoleService roleService;

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="operationLogService"></param>
        public ImpeachReportEventModule(OperationLogService operationLogService, RoleService roleService)
        {
            this.operationLogService = operationLogService;
            this.roleService = roleService;
        }

        /// <summary>
        /// 注册事件处理程序
        /// </summary>
        public void RegisterEventHandler()
        {
            EventBus<ImpeachReport>.Instance().After += new CommonEventHandler<ImpeachReport, CommonEventArgs>(ImpeachReportForOperationLog_After);
        }

        /// <summary>
        /// 生成操作日志事件
        /// </summary>
        /// <param name="thread"></param>
        /// <param name="eventArgs"></param>
        private void ImpeachReportForOperationLog_After(ImpeachReport impeachReport, CommonEventArgs eventArgs)
        {
            OperationLog newLog = new OperationLog(eventArgs.OperatorInfo);
            newLog.OperationObjectId = impeachReport.Id;
            var impeachUrlGetter = ImpeachUrlGetterFactory.Get(impeachReport.TenantTypeId);
            var impeachObject = impeachUrlGetter.GetImpeachObject(impeachReport.ReportObjectId);
            newLog.OperationObjectName = "<a class=\"a\" target=\"_blank\" href=\"" + impeachObject.DetailUrl + "\">" + impeachObject.Name + "</a>";
            newLog.OperationType = eventArgs.EventOperationType;
            newLog.TenantTypeId = TenantTypeIds.Instance().Thread();
            newLog.OperationUserRole = string.Join(",", roleService.GetRoleNamesOfUser(eventArgs.OperatorInfo.OperationUserId));
            newLog.Description = "处理举报：" + impeachObject.Name;
            operationLogService.Create(newLog);
        }
    }
}