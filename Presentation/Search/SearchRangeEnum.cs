﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------
namespace Tunynet.Search
{
    /// <summary>
    /// 搜索范围
    /// </summary>
    public enum SearchRange
    {
        /// <summary>
        /// 全部
        /// </summary>
        ALL = 0,

        /// <summary>
        /// 标题
        /// </summary>
        SUBJECT = 1,

        /// <summary>
        /// 全文
        /// </summary>
        BODY = 2,

        /// <summary>
        /// 作者
        /// </summary>
        AUTHOR = 3,

        /// <summary>
        /// 标签
        /// </summary>
        TAG = 4
    }
}